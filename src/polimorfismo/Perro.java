package polimorfismo;

public class Perro extends Mascota{
    public void ladrar(){
        System.out.println("guau");
    }

    @Override
    void pedir_comida() {
        ladrar();
    }
}
