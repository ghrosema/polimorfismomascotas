package polimorfismo;

public class Gato extends Mascota{

    public void maullar(){
        System.out.println("miauuuu");
    }

    @Override
    void pedir_comida() {
        maullar();
    }
    
}
