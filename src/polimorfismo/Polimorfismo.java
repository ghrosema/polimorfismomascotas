package polimorfismo;

import java.util.ArrayList;

public class Polimorfismo {

    public static void main(String[] args) {
        Gato michifus = new Gato();
        Perro pichi = new Perro();
        
        ArrayList<Mascota> lista = new ArrayList<>();
        
        lista.add(michifus);
        lista.add(pichi);
        
        System.out.println(lista.get(0).toString());
        System.out.println(lista.get(1).toString());
        
        lista.get(0).pedir_comida();
        lista.get(1).pedir_comida();
    }
    
}
